from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoItem, TodoList


def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    # todo_items=get_object_or_404(TodoItem, items=id)
    context = {
        "todo_list": todo_list,
        # "todo_items:": todo_items
    }
    return render(request, "todos/detail.html", context)
